const express = require("express");
const mongoose = require("mongoose");
const port = 4000;
const cors = require("cors");

const app = express();

// Connect to our mongoDB database

mongoose.connect("mongodb+srv://jarlosjoseph:admin123@batch204-jarlosjoseph.ahjdzv3.mongodb.net/s37-s41?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on ("error", () => console.error.bind(console, "Error"));

db.once("open", () => console.log("Now connected to MongoDB Atlas!"));

app.use(cors());
app.use(express.json());

app.listen(port, () => {
	console.log(`API is now online on port ${port}`);
});